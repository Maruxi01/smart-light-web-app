import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/RegisterView.vue')
    },
    {
      path: '/lights-list',
      name: 'lights-list',
      component: () => import('../views/LightsListView.vue')
    },
    {
      path: '/add-light',
      name: 'add-light',
      component: () => import('../views/AddLightView.vue')
    },
    {
      path: '/lights-stats/:id',
      name: 'lights-stats',
      component: () => import('../views/LightsStatsView.vue')
    },
    {
      path: '/lights-options',
      name: 'lights-options',
      component: () => import('../views/OptionsView.vue')

    }
  ]
})
export default router
